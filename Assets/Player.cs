﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public LayerMask layer;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, layer))
            {
                Cube cubeToBreak = hit.transform.GetComponentInParent<Cube>();
                if (cubeToBreak != null)
                {
                    cubeToBreak.Break();
                }
            }
        }
    }
}
