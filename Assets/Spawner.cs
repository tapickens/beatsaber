﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] cubes;
    public Transform[] spawnPoints;
    public float beat = 1f;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // if beat seconds have passed
        if (timer > beat)
        {
            // Create a block
            GameObject cube = Instantiate(cubes[Random.Range(0, 2)], spawnPoints[Random.Range(0, 2)]);
            cube.transform.localPosition = new Vector3(0, 0, 0);
            cube.transform.Rotate(transform.forward, 90 * Random.Range(0, 4));
            timer -= beat;
            Debug.Log("Spawn cube");
        }

        timer += Time.deltaTime;
    }
}
