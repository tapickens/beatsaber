﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public float moveSpeed = 3f;
    public GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Time.deltaTime * transform.forward * moveSpeed;

        // clean up cubes by destroying them as they pass the player
        CheckIfShouldDestroy();
    }

    private void CheckIfShouldDestroy()
    {
        if (transform.position.z > Camera.main.transform.position.z)
        {
            Destroy(this.gameObject);
        }
    }

    public void Break()
    {
        GameObject particles = Instantiate(explosion);
        particles.transform.position = this.transform.position;
        Destroy(particles, 2f); // destroy particles after 2 seconds
        Destroy(this.gameObject);
    }
}
